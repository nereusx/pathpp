\# exec: groff pathpp.man -Tascii -man | less
\#
\# .TH cmd-name section [date [version [page-descr]]]
.TH path++ 1 "2022-10-7" "NDC" "User Commands"
.
.SH NAME
path++ \- cleanup path
.SH SYNOPSIS
.SY path++
.OP \-s
.OP \-c
.OP \-b
.OP \-e varname
.OP \-r
.OP \-h
.OP \-v
.RI [ directory .\|.\|.\ \]
.YS
.SH DESCRIPTION
Check directories of path removes non-existing, duplicated, and add directories to system's PATH or other envirment variable.
With few words, cleanups the PATH. When it runs, it prints out the corrected path.
Similar variables can be used, such as MANPATH and GRPATH.
.PP
Example (tcsh):
.EX
	# clean up path
	setenv PATH `path++`
	
	# clean up MANPATH after add the /usr/local/myman
	# use only real path names, not linked pseudo-directories
	setenv MANPATH `path++ -r -e MANPATH /usr/local/myman`
.EE
.PP
Example (bash):
.EX
	# clean up path
	export PATH=$(path++)
	
	# make your path as you wish without mess it up
	NEWPATH=/bin:/usr/bin:/usr/local/bin:$PATH
	export PATH=$(path++ $NEWPATH)
.EE
.
.SH OPTIONS
.TP
.BI \-s
Add new directories at the beginning; otherwise appends
.TP
.BI \-e\  varname
Selects variable name to check, default is PATH
.TP
.BI \-r
Use only real directories.
.TP
.BI \-c
Print csh command
.TP
.BI \-b
Print sh command
.TP
.BI \-h
Help screen
.TP
.BI \-v
Program information (version, license, ...)
.
.SH SEE ALSO
.BR realpath (1),
.BR stat (1).
.
.SH AUTHOR
.MT netnic@\:proton.me
Nicholas Christopoulos
.ME
.br
.UR https://codeberg.com/nereusx/pathpp
Project page:
.UE
.
.SH COPYRIGHT
Copyright © 2020 - 2025 Free Software Foundation, Inc.
.br
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
.br
This is free software: you are free to change and redistribute it.
.br
There is NO WARRANTY, to the extent permitted by law.
\# EOF
