# path++

Check directories of path removes non-existing, duplicated,
and adds directories to system's PATH or other envirment variable.

## Examples

```
# clean up path
export PATH=$(path++)
	
# make your path as you wish without mess it up
NEWPATH=/bin:/usr/bin:/usr/local/bin:$PATH
export PATH=$(path++ $NEWPATH)

# clean up MANPATH after add the /usr/local/myman
# use only real path names, not linked pseudo-directories
export MANPATH=$(path++ -r -e MANPATH /usr/local/myman)
```

## Installation

```
git clone https://codeberg.org/nereusx/pathpp
cd pathpp
make && make install
```

[Read man page](https://codeberg.org/nereusx/pathpp/src/branch/main/path++.pdf)
